from django.db import models

class Person(models.Model):
    name = models.CharField(max_length=255)
    age = models.PositiveIntegerField()

class Address(models.Model):
    city = models.CharField(max_length=255)
    country = models.CharField(max_length=255, default="antartic")
    owner = models.ForeignKey(Person, on_delete=models.CASCADE)
