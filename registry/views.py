from django.http import HttpResponse
from django.template import loader
from registry.models import Person

def test(request):
    template = loader.get_template("test.html")
    people = Person.objects.all()
    return HttpResponse(template.render({"people": people}, request))